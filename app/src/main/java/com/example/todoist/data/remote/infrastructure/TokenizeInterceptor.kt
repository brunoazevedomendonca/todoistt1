package com.example.todoist.data.remote.infrastructure

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

class TokenizeInterceptor @Inject constructor() : Interceptor {

    // TODO - Configurar token:
    //  acessar https://developer.todoist.com/appconsole.html
    //  criar conta
    //  criar app
    //  criar test token
    companion object {
        private const val API_TEST_TOKEN = "c9ec57f52e4fe1e4fe4b8f1fe7fc86f5935627d2"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest: Request = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer $API_TEST_TOKEN")
            .build()
        return chain.proceed(newRequest)
    }
}