package com.example.todoist.presentation.scene.tasks

import android.view.View
import com.example.todoist.R
import com.example.todoist.data.model.Task
import com.example.todoist.databinding.CheckItemBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.viewbinding.BindableItem

class TasksAdapter : GroupAdapter<GroupieViewHolder>() {

    fun setItems(taskList: List<Task>) {
        taskList.forEach { add(TaskItem(it)) }
    }

    internal class TaskItem(private val task: Task) : BindableItem<CheckItemBinding>() {
        override fun bind(viewBinding: CheckItemBinding, position: Int) {
            viewBinding.checkItem.text = task.content
        }

        override fun getLayout(): Int = R.layout.check_item

        override fun initializeViewBinding(view: View): CheckItemBinding =
            CheckItemBinding.bind(view)
    }

}