package com.example.todoist.presentation.scene.tasks

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todoist.R
import com.example.todoist.common.TodoistApplication
import com.example.todoist.databinding.FragmentTasksBinding
import com.example.todoist.presentation.common.ScreenState
import com.example.todoist.presentation.common.ViewModelFactory
import com.example.todoist.presentation.scene.sections.SectionsAdapter
import com.example.todoist.presentation.scene.sections.SectionsFragment
import javax.inject.Inject

class TasksFragment : Fragment() {

    companion object {
        private const val SECTION_ID_KEY = "SECTION_ID_KEY"

        fun newInstance(sectionId: Long) = TasksFragment().apply {
            arguments = Bundle().apply {
                putLong(SECTION_ID_KEY, sectionId)
            }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var binding: FragmentTasksBinding
    private lateinit var viewModel: TasksViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as TodoistApplication).applicationComponent.inject(this)
        viewModel = ViewModelProvider(this, viewModelFactory).get(TasksViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTasksBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sectionId = requireArguments().getLong(SECTION_ID_KEY)
        viewModel.onSectionIdReceived(sectionId)

        val adapter = TasksAdapter()

        binding.taskList.adapter = adapter
        binding.taskList.layoutManager = LinearLayoutManager(requireContext())

        viewModel.screenState.observe(this) { screenState ->
            when (screenState) {
                is ScreenState.Success -> {
                    adapter.setItems(screenState.data)
                    binding.emptyStateIndicator.visibility = View.GONE
                    binding.progressIndicator.visibility = View.GONE
                    binding.taskList.visibility = View.VISIBLE
                }
                is ScreenState.Loading -> {
                    binding.emptyStateIndicator.visibility = View.GONE
                    binding.taskList.visibility = View.GONE
                    binding.progressIndicator.visibility = View.VISIBLE
                }
                is ScreenState.Error -> {
                    binding.taskList.visibility = View.GONE
                    binding.progressIndicator.visibility = View.GONE
                    binding.emptyStateIndicator.visibility = View.VISIBLE
                }
            }
        }
    }
}