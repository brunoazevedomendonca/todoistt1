package com.example.todoist.presentation.scene.sections

import android.view.View
import com.example.todoist.R
import com.example.todoist.data.model.Section
import com.example.todoist.databinding.SimpleTextItemBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.viewbinding.BindableItem

class SectionsAdapter(
    private val onItemClicked: (Section) -> Unit
) : GroupAdapter<GroupieViewHolder>() {

    fun setItems(sectionList: List<Section>) {
        sectionList.forEach { add(SectionItem(it)) }
    }

    inner class SectionItem(private val section: Section) : BindableItem<SimpleTextItemBinding>() {
        override fun bind(viewBinding: SimpleTextItemBinding, position: Int) {
            viewBinding.textItem.text = section.name

            viewBinding.root.setOnClickListener {
                onItemClicked(section)
            }
        }

        override fun getLayout(): Int = R.layout.simple_text_item

        override fun initializeViewBinding(view: View): SimpleTextItemBinding =
            SimpleTextItemBinding.bind(view)
    }

}